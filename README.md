# CapcoAssignment

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory

## Lint
Run `ng lint` to lint the project. Linting will find any formatting issues within the code base.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma]

## Features 
Home page contains all features that are designated in the CAPCO assignment.

    1. The component should display Sample Data in a table

    2. User should be able to select how many rows are displayed in the table

    3. Table should be paginated if not all rows are displayed on the screen based on the user’s selection

    4. Pagination options should be displayed in the table footer

    5. Column names should be displayed in the table header

    6. Entire table, table header and table footer should always be displayed on the screen while scrolling

    7. If number of rows exceeds the size of the table body, a vertical scrollbar should be displayed within the table body – only table body shall scroll vertically, table header and footer shall remain as is

    8. If number of columns exceed the size of the table body, a horizontal scrollbar should be displayed within the table body – only table body and table header shall scroll to reveal the additional columns, table footer shall remain as is

    9. Each row should contain a button which shall submit the row ID and row status to /api/submit as a POST request – You are not expected to create the POST endpoint, but you can mock one if you like

Extra page contains the specified features

    1. No pagination

    2. Infinite vertical scroll that loads and unloads rows into the DOM as required (feature not complete)
    
    3. Horizontal scroll based on number of columns
