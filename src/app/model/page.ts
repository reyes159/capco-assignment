export class Page {
    PageNumber: number;
    PageSize: number;


    constructor(pageNumber: number, pageSize: number) {
        this.PageNumber = pageNumber;
        if (pageSize) {
            this.PageSize = pageSize;
        }
    }
}
