import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppLayoutComponent } from './shared/layout';
import { HomeComponent, PageNotFoundComponent, ExtraComponent } from './views';


const routes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    data: {
    },
    children: [{
      path: '',
      redirectTo: 'home',
      pathMatch: 'full'
    },
    {
      path: 'home',
      component: HomeComponent,
    },
    {
      path: 'extra',
      component: ExtraComponent,
    }
  ]
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
