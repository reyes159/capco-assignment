import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtraComponent } from './extra.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DATATABLE_COMPONENTS } from 'src/app/shared/data-table';

describe('ExtraComponent', () => {
  let component: ExtraComponent;
  let fixture: ComponentFixture<ExtraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ ExtraComponent, DATATABLE_COMPONENTS ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
