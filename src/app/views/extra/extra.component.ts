import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import sampleData from './../../../assets/sample_data.json';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-extra',
  templateUrl: './extra.component.html',
  styleUrls: ['./extra.component.scss']
})
export class ExtraComponent implements OnInit {
  rawList = Array<any>();
  hasSubmitColumn = false;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.rawList = sampleData;
  }

  submit(data: any) {
    if (data) {
      const postObject = {
        id: data.id,
        status: data.status
      };
      this.postObject(postObject).subscribe(response => console.log(response));
    }
  }

  postObject(data: any): Observable<any> {
    return this.http.post('/api/submit', JSON.stringify(data));
  }
}
