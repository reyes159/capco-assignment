import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ExtraComponent } from './extra/extra.component';


export * from './home/home.component';
export * from './extra/extra.component';
export * from './page-not-found/page-not-found.component';

export const VIEW_COMPONENTS = [
    HomeComponent,
    PageNotFoundComponent,
    ExtraComponent
];
