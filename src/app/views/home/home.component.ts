import { Component, OnInit } from '@angular/core';
import sampleData from './../../../assets/sample_data.json';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  rawList = Array<any>();
  hasSubmitColumn = true;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.rawList = sampleData;
  }

  submit(data: any) {
    if (data) {
      const postObject = {
        id: data.id,
        status: data.status
      };
      this.postObject(postObject).subscribe(response => console.log(response));
    }
  }

  postObject(data: any): Observable<any> {
    return this.http.post('/api/submit', JSON.stringify(data));
  }
}
