import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Page } from 'src/app/model/page';
@Component({
  selector: 'app-data-table-container',
  templateUrl: './data-table-container.component.html',
  styleUrls: ['./data-table-container.component.scss']
})
export class DataTableContainerComponent implements OnInit {

  @Input() rawList = Array<any>();
  @Input() hasSubmitColumn: boolean;
  @Output() submissionRequested: EventEmitter<any> = new EventEmitter<any>();
  @Input() enablePaging = true;

  dataList = Array<any>();

  columns = Array<string>();
  pageSizeOptions = [10, 20, 50, 100];
  pageSize = 20;
  currentPage = 1;


  constructor() { }

  ngOnInit() {
    if (this.rawList != null && this.rawList.length > 0) {
      this.columns = Object.keys(this.rawList[0]);
      this.loadPage(new Page(this.currentPage, this.pageSize));
    }
  }

  loadPage(page: Page) {
    if (this.enablePaging) {
      if (page.PageSize) {
        this.pageSize = page.PageSize;
      }
      if (!isNaN(page.PageNumber)) {
        this.currentPage = page.PageNumber;

        const begin = ((page.PageNumber - 1) * this.pageSize);
        const end = begin + this.pageSize;
        this.dataList = this.rawList.slice(begin, end);
      }
    } else {
      this.dataList = this.rawList;
    }
  }

  submit(data: any) {
    if (data) {
      this.submissionRequested.emit(data);
    }
  }
}
