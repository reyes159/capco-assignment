import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataTableContainerComponent } from './data-table-container.component';
import { DATATABLE_COMPONENTS } from '..';

describe('DataTableContainerComponent', () => {
  let component: DataTableContainerComponent;
  let fixture: ComponentFixture<DataTableContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataTableContainerComponent, DATATABLE_COMPONENTS ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTableContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
