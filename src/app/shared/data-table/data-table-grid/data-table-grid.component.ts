import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'tbody[data-table-grid]',
  templateUrl: './data-table-grid.component.html',
  styleUrls: ['./data-table-grid.component.scss']
})
export class DataTableGridComponent implements OnInit {
  @Input() columns = Array<string>();
  @Input() dataList = Array<any>();
  @Input() hasSubmitColumn: boolean;

  @Output() submissionRequested: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  submit(data: any) {
    if (data) {
      this.submissionRequested.emit(data);
    }
  }

}
