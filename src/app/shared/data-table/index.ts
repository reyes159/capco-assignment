import { DataTableContainerComponent } from './data-table-container/data-table-container.component';
import { DataTableHeaderComponent } from './data-table-header/data-table-header.component';
import { DataTableGridComponent } from './data-table-grid/data-table-grid.component';
import { DataTableFooterComponent } from './data-table-footer/data-table-footer.component';

export * from './data-table-container/data-table-container.component';
export * from './data-table-header/data-table-header.component';
export * from  './data-table-grid/data-table-grid.component';
export * from './data-table-footer/data-table-footer.component';

export const DATATABLE_COMPONENTS = [
DataTableContainerComponent,
DataTableHeaderComponent,
DataTableGridComponent,
DataTableFooterComponent
];
