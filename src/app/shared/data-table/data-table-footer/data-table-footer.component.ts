import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Page } from 'src/app/model/page';

@Component({
  selector: 'app-data-table-footer',
  templateUrl: './data-table-footer.component.html',
  styleUrls: ['./data-table-footer.component.scss']
})
export class DataTableFooterComponent implements OnInit {
  @Input() pageSizeOptions = Array<number>();

  @Input() page: number;
  @Input() collectionSize: number;
  @Input() pageSize: number;
  @Input() maxSize = Array<number>();
  @Input() enablePaging: boolean;
  @Output() pageChange: EventEmitter<Page> = new EventEmitter<Page>();


  constructor() { }

  ngOnInit() {
    if (this.collectionSize && this.pageSize) {
      const pageNumbers = Math.floor(this.collectionSize / this.pageSize);
      this.maxSize = Array.from(Array(pageNumbers + 1).keys()).splice(1);
    }

  }


  loadPage(page: number, pageSize?: number) {
    if (!isNaN(page)) {
      this.pageChange.emit(new Page(page, pageSize ? pageSize : this.pageSize));
    }
  }

  changePageSize(pageSize: number) {
    if (!isNaN(pageSize)) {
      this.loadPage(1, pageSize);
      this.pageSize = pageSize;
      const pageNumbers = Math.floor(this.collectionSize / this.pageSize);
      this.maxSize = Array.from(Array(pageNumbers + 1).keys()).splice(1);
    }
  }
}

