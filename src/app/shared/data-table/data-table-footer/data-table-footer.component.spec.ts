import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataTableFooterComponent } from './data-table-footer.component';

describe('DataTableFooterComponent', () => {
  let component: DataTableFooterComponent;
  let fixture: ComponentFixture<DataTableFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataTableFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTableFooterComponent);
    component = fixture.componentInstance;
    component.collectionSize = 200;
    component.pageSize = 20;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a max pages', () => {
    console.log('CollectionSize = ', component.collectionSize );
    console.log('PageSize = ', component.pageSize );
    console.log('Max Pages = ', component.maxSize.length );
    console.log('Max Pages should be 10 ' );

    expect(component.maxSize.length === 10 ).toBeTruthy();
    });
});
