import { Component, OnInit, Input } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'thead[data-table-header]',
  templateUrl: './data-table-header.component.html',
  styleUrls: ['./data-table-header.component.scss']
})
export class DataTableHeaderComponent implements OnInit {
  @Input() columns = Array<string>();
  @Input() hasSubmitColumn: boolean;
  constructor() { }

  ngOnInit() {
    if (this.columns != null && this.columns.length > 0) {
      this.columns = this.columns.map(key => key.charAt(0).toUpperCase() + key.slice(1));
    }
  }

}
