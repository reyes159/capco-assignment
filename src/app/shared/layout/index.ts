import { AppLayoutComponent } from './app-layout/app-layout.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { HeaderComponent } from './header/header.component';

export * from './app-layout/app-layout.component';
export * from './header/header.component';
export * from './side-bar/side-bar.component';

export const LAYOUT_COMPONENTS = [
    AppLayoutComponent,
    HeaderComponent,
    SideBarComponent
];
