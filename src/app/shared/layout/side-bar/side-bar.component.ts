import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  @Input() routes: Array<string>;
  activeRoute: string;
  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.setSelectionBasedOnRoute();
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.setSelectionBasedOnRoute();
      }
    });
  }

  private setSelectionBasedOnRoute() {
    if (this.route) {
      const currentRoute = this.router.routerState.snapshot.url.split('/');
      this.activeRoute = currentRoute[1];
    }
  }

  isRouteActive(route: string) {
    if (route && this.formatRoute(route) === this.activeRoute) {
      return true;
    }
    return false;
  }

  routeToLink(route: string) {
    if (route) {
      const url = this.formatRoute(route);
      this.router.navigateByUrl(url);
    }
  }

  formatRoute(route: string) {
    return route.toLowerCase().replace(' ', '-');
  }
}
