import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LAYOUT_COMPONENTS } from './shared/layout';
import { VIEW_COMPONENTS } from './views';
import { DATATABLE_COMPONENTS } from './shared/data-table';
import { HttpClientModule } from '@angular/common/http';
import { ExtraComponent } from './views/extra/extra.component';

@NgModule({
  declarations: [
    AppComponent,
    LAYOUT_COMPONENTS,
    VIEW_COMPONENTS,
    DATATABLE_COMPONENTS,
    ExtraComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
